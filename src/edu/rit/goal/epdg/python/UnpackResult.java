package edu.rit.goal.epdg.python;

import java.util.Set;

public class UnpackResult {

	private String fullExpression;
	private String partialExpression;
	private String dependentVar;
	private Set<String> independentVars;

	public UnpackResult(String fullExpression, String partialExpression, String dependentVar,
			Set<String> independentVars) {
		this.fullExpression = fullExpression;
		this.partialExpression = partialExpression;
		this.dependentVar = dependentVar;
		this.independentVars = independentVars;
	}

	public String getFullExpression() {
		return fullExpression;
	}

	public void setFullExpression(String fullExpression) {
		this.fullExpression = fullExpression;
	}

	public String getPartialExpression() {
		return partialExpression;
	}

	public void setPartialExpression(String partialExpression) {
		this.partialExpression = partialExpression;
	}

	public String getDependentVar() {
		return dependentVar;
	}

	public void setDependentVar(String dependentVar) {
		this.dependentVar = dependentVar;
	}

	public Set<String> getIndependentVars() {
		return independentVars;
	}

	public void setIndependentVars(Set<String> independentVars) {
		this.independentVars = independentVars;
	}

	@Override
	public String toString() {
		return "'" + getDependentVar() + "'" + " <- " + getIndependentVars();
	}

}

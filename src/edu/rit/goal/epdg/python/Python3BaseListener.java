package edu.rit.goal.epdg.python;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

// Generated from Python3.g4 by ANTLR 4.5.3

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import edu.rit.goal.epdg.object.Edge;
import edu.rit.goal.epdg.object.EdgeType;
import edu.rit.goal.epdg.object.Vertex;
import edu.rit.goal.epdg.object.VertexType;
import edu.rit.goal.epdg.python.antlr.Python3Listener;
import edu.rit.goal.epdg.python.antlr.Python3Parser;
import edu.rit.goal.epdg.python.antlr.Python3Parser.Expr_stmtContext;
import edu.rit.goal.epdg.python.antlr.Python3Parser.Testlist_star_exprContext;
import edu.rit.goal.epdg.python.wrapper.ForStmtContextWrapper;
import edu.rit.goal.epdg.python.wrapper.FuncdefContextWrapper;
import edu.rit.goal.epdg.python.wrapper.IfStmtContextWrapper;
import edu.rit.goal.epdg.python.wrapper.ReturnStmtContextWrapper;
import edu.rit.goal.epdg.python.wrapper.StmtContextWrapper;
import edu.rit.goal.epdg.python.wrapper.WhileStmtContextWrapper;

/**
 * This class provides an empty implementation of {@link Python3Listener}, which
 * can be extended to create a listener which only needs to handle a subset of
 * the available methods.
 */
public class Python3BaseListener implements Python3Listener {

	// TODO 0: Need to take the scope of variables into account!

	private String currentMethod = "";

	private final Map<String, List<Vertex>> vertices = new HashMap<>();
	private final Map<String, List<Edge>> edges = new HashMap<>();

	private final Map<String, Vertex> lastAppearanceOfVariables = new HashMap<>();
	private int vertexCounter;
	private final Stack<Vertex> controlStack = new Stack<>();

	public Python3BaseListener() {
	}

	// AUX

	private void print(String[] str) {
		Arrays.stream(str).forEach(s -> System.out.println(s));
		System.out.println("=================================");
	}

	private Vertex createVertex(VertexType type, String lbl, String assignedVar, Set<String> refVars) {
		final Vertex v = new Vertex(vertexCounter++);
		v.setAssignedVariable(assignedVar);
		v.setType(type);
		v.setLabel(lbl);
		v.getReadingVariables().addAll(refVars);
		vertices.get(currentMethod).add(v);

		if (!controlStack.isEmpty())
			createEdge(controlStack.peek(), v, EdgeType.CTRL);

		return v;
	}

	private Edge createEdge(Vertex from, Vertex to, EdgeType type) {
		final Edge e = new Edge(from, to, type);
		edges.get(currentMethod).add(e);
		return e;
	}

	private void createDataEdges(Vertex v, Set<String> expVars) {
		for (final String var : expVars) {
			final Vertex lastAppV = lastAppearanceOfVariables.get(var);
			if (lastAppV != null)
				createEdge(lastAppV, v, EdgeType.DATA);
		}
	}

	// Supported rules

	@Override
	public void enterFuncdef(Python3Parser.FuncdefContext ctx) {
		final FuncdefContextWrapper cw = new FuncdefContextWrapper(ctx);
		cw.funcDef();
		currentMethod = cw.getFuncName();
		vertices.put(currentMethod, new ArrayList<>());
		edges.put(currentMethod, new ArrayList<>());
		lastAppearanceOfVariables.clear();
		vertexCounter = 0;
		controlStack.clear();
		final Set<String> params = cw.getParameters();
		params.stream().forEach(p -> {
			final Vertex v = createVertex(VertexType.DECL, p, p, new HashSet<>());
			lastAppearanceOfVariables.put(p, v);
		});
	}

	@Override
	public void exitFuncdef(Python3Parser.FuncdefContext ctx) {
	}

	@Override
	public void enterWhile_stmt(Python3Parser.While_stmtContext ctx) {
		final WhileStmtContextWrapper cw = new WhileStmtContextWrapper(ctx);
		cw.build();
		print(cw.str());
		final Set<String> conditionVars = cw.getConditionVars();
		final Vertex v = createVertex(VertexType.CTRL, cw.getConditionText(), null, conditionVars);
		createDataEdges(v, conditionVars);
		controlStack.push(v);
		// Create self edge.
		createEdge(v, v, EdgeType.CTRL);
	}

	@Override
	public void exitWhile_stmt(Python3Parser.While_stmtContext ctx) {
		controlStack.pop();
	}

	@Override
	public void enterFor_stmt(Python3Parser.For_stmtContext ctx) {
		final ForStmtContextWrapper cw = new ForStmtContextWrapper(ctx);
		cw.build();
		print(cw.str());
		final Set<String> inVars = cw.getInVars();
		final Vertex v = createVertex(VertexType.CTRL, cw.getText(), null, inVars);
		createDataEdges(v, inVars);
		controlStack.push(v);
		// Create self edge.
		createEdge(v, v, EdgeType.CTRL);

		final String varAssigned = cw.getVarAssigned();
		final Vertex exprListNode = createVertex(VertexType.DECL, ctx.exprlist().getText(), varAssigned, inVars);
		lastAppearanceOfVariables.put(varAssigned, exprListNode);
	}

	@Override
	public void exitFor_stmt(Python3Parser.For_stmtContext ctx) {
		controlStack.pop();
	}

	@Override
	public void enterExpr_stmt(Python3Parser.Expr_stmtContext ctx) {
		dealWithExprStmt(ctx);
	}

	@Override
	public void exitExpr_stmt(Expr_stmtContext ctx) {
	}

	@Override
	public void enterIf_stmt(Python3Parser.If_stmtContext ctx) {
		final IfStmtContextWrapper cw = new IfStmtContextWrapper(ctx);
		cw.build();
		print(cw.str());
		final Set<String> conditionVars = cw.getConditionVars();
		final Vertex v = createVertex(VertexType.CTRL, cw.getConditionText(), null, conditionVars);
		createDataEdges(v, conditionVars);
		controlStack.push(v);
		// Create self edge.
		createEdge(v, v, EdgeType.CTRL);
	}

	@Override
	public void exitIf_stmt(Python3Parser.If_stmtContext ctx) {
		controlStack.pop();
	}

	// TODO: Deal with variable unpacking
	private void dealWithExprStmt(Python3Parser.Expr_stmtContext ctx) {
		Set<String> expVars = null;
		String varAssigned;

		final StmtContextWrapper cw = new StmtContextWrapper(ctx);

		final List<Testlist_star_exprContext> tlseCtxt = ctx.testlist_star_expr();
		final String leftHandSideTxt = tlseCtxt.get(0).getText();

		final String fullExpr = ctx.getText();
		System.out.println("Full expression: " + fullExpr);

		switch (cw.exprStmt()) {
		case StmtContextWrapper.ASSIGN:
			final List<UnpackResult> unpacked = cw.getUnpacked();
			for (final UnpackResult ur : unpacked) {
				varAssigned = ur.getDependentVar();
				expVars = ur.getIndependentVars();
				final Vertex v = createVertex(VertexType.ASSIGN, ctx.getText(), varAssigned, expVars);
				createDataEdges(v, expVars);
				lastAppearanceOfVariables.put(varAssigned, v);
			}
			break;
		case StmtContextWrapper.AUG_ASSIGN:
			varAssigned = cw.getVarAssigned();
			expVars = cw.getExpVars();
			expVars.add(varAssigned);
			final Vertex vAug = createVertex(VertexType.ASSIGN, ctx.getText(), varAssigned, expVars);
			createDataEdges(vAug, expVars);
			lastAppearanceOfVariables.put(varAssigned, vAug);
			System.out.println("Left-hand side: " + leftHandSideTxt);
			System.out.println("Depends on vars: " + expVars.toString());
			break;
		case StmtContextWrapper.FUNC_CALL:
			expVars = cw.getExpVars();
			final Vertex vCall = createVertex(VertexType.CALL, ctx.getText(), null, expVars);
			createDataEdges(vCall, expVars);
			if (vCall.getAssignedVariable() != null)
				lastAppearanceOfVariables.put(vCall.getAssignedVariable(), vCall);
			System.out.println("Left-hand side: " + leftHandSideTxt);
			System.out.println("Depends on vars: " + expVars.toString());
			break;
		}

		System.out.println("=================================");
	}

	// Unsupported rules

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSingle_input(Python3Parser.Single_inputContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSingle_input(Python3Parser.Single_inputContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFile_input(Python3Parser.File_inputContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFile_input(Python3Parser.File_inputContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEval_input(Python3Parser.Eval_inputContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEval_input(Python3Parser.Eval_inputContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDecorator(Python3Parser.DecoratorContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDecorator(Python3Parser.DecoratorContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDecorators(Python3Parser.DecoratorsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDecorators(Python3Parser.DecoratorsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDecorated(Python3Parser.DecoratedContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDecorated(Python3Parser.DecoratedContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterParameters(Python3Parser.ParametersContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitParameters(Python3Parser.ParametersContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypedargslist(Python3Parser.TypedargslistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypedargslist(Python3Parser.TypedargslistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTfpdef(Python3Parser.TfpdefContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTfpdef(Python3Parser.TfpdefContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterVarargslist(Python3Parser.VarargslistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitVarargslist(Python3Parser.VarargslistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterVfpdef(Python3Parser.VfpdefContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitVfpdef(Python3Parser.VfpdefContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterStmt(Python3Parser.StmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitStmt(Python3Parser.StmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSimple_stmt(Python3Parser.Simple_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSimple_stmt(Python3Parser.Simple_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSmall_stmt(Python3Parser.Small_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSmall_stmt(Python3Parser.Small_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTestlist_star_expr(Python3Parser.Testlist_star_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTestlist_star_expr(Python3Parser.Testlist_star_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAugassign(Python3Parser.AugassignContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAugassign(Python3Parser.AugassignContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDel_stmt(Python3Parser.Del_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDel_stmt(Python3Parser.Del_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPass_stmt(Python3Parser.Pass_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPass_stmt(Python3Parser.Pass_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFlow_stmt(Python3Parser.Flow_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFlow_stmt(Python3Parser.Flow_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterBreak_stmt(Python3Parser.Break_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitBreak_stmt(Python3Parser.Break_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterContinue_stmt(Python3Parser.Continue_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitContinue_stmt(Python3Parser.Continue_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterReturn_stmt(Python3Parser.Return_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitReturn_stmt(Python3Parser.Return_stmtContext ctx) {
		final ReturnStmtContextWrapper cw = new ReturnStmtContextWrapper(ctx);
		cw.build();
		Vertex v = createVertex(VertexType.RETURN, ctx.getText(), null, cw.getDepVars());
		createDataEdges(v, cw.getDepVars());
		System.out.println("Full expression: " + ctx.getText());
		System.out.println("Depends on vars: " + cw.getDepVars().toString());
		System.out.println("=================================");
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterYield_stmt(Python3Parser.Yield_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitYield_stmt(Python3Parser.Yield_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterRaise_stmt(Python3Parser.Raise_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitRaise_stmt(Python3Parser.Raise_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterImport_stmt(Python3Parser.Import_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitImport_stmt(Python3Parser.Import_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterImport_name(Python3Parser.Import_nameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitImport_name(Python3Parser.Import_nameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterImport_from(Python3Parser.Import_fromContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitImport_from(Python3Parser.Import_fromContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterImport_as_name(Python3Parser.Import_as_nameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitImport_as_name(Python3Parser.Import_as_nameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDotted_as_name(Python3Parser.Dotted_as_nameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDotted_as_name(Python3Parser.Dotted_as_nameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterImport_as_names(Python3Parser.Import_as_namesContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitImport_as_names(Python3Parser.Import_as_namesContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDotted_as_names(Python3Parser.Dotted_as_namesContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDotted_as_names(Python3Parser.Dotted_as_namesContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDotted_name(Python3Parser.Dotted_nameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDotted_name(Python3Parser.Dotted_nameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterGlobal_stmt(Python3Parser.Global_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitGlobal_stmt(Python3Parser.Global_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterNonlocal_stmt(Python3Parser.Nonlocal_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitNonlocal_stmt(Python3Parser.Nonlocal_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAssert_stmt(Python3Parser.Assert_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAssert_stmt(Python3Parser.Assert_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterCompound_stmt(Python3Parser.Compound_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitCompound_stmt(Python3Parser.Compound_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTry_stmt(Python3Parser.Try_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTry_stmt(Python3Parser.Try_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterWith_stmt(Python3Parser.With_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitWith_stmt(Python3Parser.With_stmtContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterWith_item(Python3Parser.With_itemContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitWith_item(Python3Parser.With_itemContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExcept_clause(Python3Parser.Except_clauseContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExcept_clause(Python3Parser.Except_clauseContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSuite(Python3Parser.SuiteContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSuite(Python3Parser.SuiteContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTest(Python3Parser.TestContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTest(Python3Parser.TestContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTest_nocond(Python3Parser.Test_nocondContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTest_nocond(Python3Parser.Test_nocondContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLambdef(Python3Parser.LambdefContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLambdef(Python3Parser.LambdefContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLambdef_nocond(Python3Parser.Lambdef_nocondContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLambdef_nocond(Python3Parser.Lambdef_nocondContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterOr_test(Python3Parser.Or_testContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitOr_test(Python3Parser.Or_testContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAnd_test(Python3Parser.And_testContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAnd_test(Python3Parser.And_testContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterNot_test(Python3Parser.Not_testContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitNot_test(Python3Parser.Not_testContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterComparison(Python3Parser.ComparisonContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitComparison(Python3Parser.ComparisonContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterComp_op(Python3Parser.Comp_opContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitComp_op(Python3Parser.Comp_opContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterStar_expr(Python3Parser.Star_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitStar_expr(Python3Parser.Star_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExpr(Python3Parser.ExprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExpr(Python3Parser.ExprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterXor_expr(Python3Parser.Xor_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitXor_expr(Python3Parser.Xor_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAnd_expr(Python3Parser.And_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAnd_expr(Python3Parser.And_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterShift_expr(Python3Parser.Shift_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitShift_expr(Python3Parser.Shift_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterArith_expr(Python3Parser.Arith_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitArith_expr(Python3Parser.Arith_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTerm(Python3Parser.TermContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTerm(Python3Parser.TermContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFactor(Python3Parser.FactorContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFactor(Python3Parser.FactorContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPower(Python3Parser.PowerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPower(Python3Parser.PowerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAtom(Python3Parser.AtomContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAtom(Python3Parser.AtomContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTestlist_comp(Python3Parser.Testlist_compContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTestlist_comp(Python3Parser.Testlist_compContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTrailer(Python3Parser.TrailerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTrailer(Python3Parser.TrailerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSubscriptlist(Python3Parser.SubscriptlistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSubscriptlist(Python3Parser.SubscriptlistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSubscript(Python3Parser.SubscriptContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSubscript(Python3Parser.SubscriptContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSliceop(Python3Parser.SliceopContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSliceop(Python3Parser.SliceopContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExprlist(Python3Parser.ExprlistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExprlist(Python3Parser.ExprlistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTestlist(Python3Parser.TestlistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTestlist(Python3Parser.TestlistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDictorsetmaker(Python3Parser.DictorsetmakerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDictorsetmaker(Python3Parser.DictorsetmakerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassdef(Python3Parser.ClassdefContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassdef(Python3Parser.ClassdefContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterArglist(Python3Parser.ArglistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitArglist(Python3Parser.ArglistContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterArgument(Python3Parser.ArgumentContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitArgument(Python3Parser.ArgumentContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterComp_iter(Python3Parser.Comp_iterContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitComp_iter(Python3Parser.Comp_iterContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterComp_for(Python3Parser.Comp_forContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitComp_for(Python3Parser.Comp_forContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterComp_if(Python3Parser.Comp_ifContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitComp_if(Python3Parser.Comp_ifContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterYield_expr(Python3Parser.Yield_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitYield_expr(Python3Parser.Yield_exprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterYield_arg(Python3Parser.Yield_argContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitYield_arg(Python3Parser.Yield_argContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterString(Python3Parser.StringContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitString(Python3Parser.StringContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterNumber(Python3Parser.NumberContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitNumber(Python3Parser.NumberContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInteger(Python3Parser.IntegerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInteger(Python3Parser.IntegerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEveryRule(ParserRuleContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void visitTerminal(TerminalNode node) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void visitErrorNode(ErrorNode node) {
	}

	public Set<String> getMethods() {
		return vertices.keySet();
	}

	public List<Vertex> getVertices(String method) {
		return vertices.get(method);
	}

	public List<Edge> getEdges(String method) {
		return edges.get(method);
	}

}
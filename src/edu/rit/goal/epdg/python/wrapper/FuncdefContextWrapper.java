package edu.rit.goal.epdg.python.wrapper;

import java.util.Set;

import edu.rit.goal.epdg.python.antlr.Python3Parser.FuncdefContext;
import edu.rit.goal.epdg.python.antlr.Python3Parser.ParametersContext;

public class FuncdefContextWrapper extends AbstractContextWrapper {

	{
		_EXCLUDE = new String[] { "\\d+", "\\*", "=", "\\(", "\\)" };
	}

	private final FuncdefContext ctx;

	private String funcName;
	private Set<String> parameters;

	public FuncdefContextWrapper(FuncdefContext ctx) {
		this.ctx = ctx;
	}

	public void funcDef() {
		final ParametersContext pc = ctx.parameters();
		funcName = ctx.NAME().getText();
		parameters = getVars(pc);
	}
	
	public String getFuncName() {
		return funcName;
	}

	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	public Set<String> getParameters() {
		return parameters;
	}

	public void setParameters(Set<String> parameters) {
		this.parameters = parameters;
	}

}

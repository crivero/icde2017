package edu.rit.goal.epdg.python.wrapper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import org.antlr.v4.runtime.tree.ParseTree;

public abstract class AbstractContextWrapper {

	protected static String[] _EXCLUDE = {};

	protected Set<String> getVars(ParseTree pt) {
		final Set<String> res = new HashSet<>();
		if (pt.getChildCount() == 0) {
			final String terminal = pt.getText();
			final Stream<String> stream = Arrays.stream(_EXCLUDE);
			final Stream<Boolean> matches = stream.map(p -> terminal.matches(p));
			if (!matches.anyMatch(b -> b == true)) {
				res.add(terminal);
			}
		} else {
			for (int i = 0; i < pt.getChildCount(); i++) {
				res.addAll(getVars(pt.getChild(i)));
			}
		}
		return res;
	}

}

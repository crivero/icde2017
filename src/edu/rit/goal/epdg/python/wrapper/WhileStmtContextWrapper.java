package edu.rit.goal.epdg.python.wrapper;

import java.util.Set;

import edu.rit.goal.epdg.python.antlr.Python3Parser.While_stmtContext;

public class WhileStmtContextWrapper extends AbstractContextWrapper {

	{
		_EXCLUDE = new String[] { "\\d+", "\\*", "=", "\\(", "\\)", "&&", "||", "<", ">", "<=", ">=", "==", "%", "len", "range" };
	}

	private final While_stmtContext ctx;

	private Set<String> conditionVars;
	private String conditionText;

	public WhileStmtContextWrapper(While_stmtContext ctx) {
		this.ctx = ctx;
	}

	public void build() {
		conditionVars = getVars(ctx.test());
		conditionText = ctx.test().getText();
	}

	public Set<String> getConditionVars() {
		return conditionVars;
	}

	public void setConditionVars(Set<String> conditionVars) {
		this.conditionVars = conditionVars;
	}

	public String getConditionText() {
		return conditionText;
	}

	public void setConditionText(String conditionText) {
		this.conditionText = conditionText;
	}

	public String[] str() {
		return new String[]{"While condition expression: " + conditionText, "Condition vars: " + conditionVars.toString()};
	}

}

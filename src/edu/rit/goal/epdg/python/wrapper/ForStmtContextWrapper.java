package edu.rit.goal.epdg.python.wrapper;

import java.util.Set;

import edu.rit.goal.epdg.python.antlr.Python3Parser.For_stmtContext;

public class ForStmtContextWrapper extends AbstractContextWrapper {

	{
		_EXCLUDE = new String[] { "\\d+", "\\*", "=", "\\(", "\\)", "&&", "||", "<", ">", "<=", ">=", "==", "%", "len",
		"range" };
	}

	private final For_stmtContext ctx;

	private String varAssigned;
	private Set<String> inVars;

	public ForStmtContextWrapper(For_stmtContext ctx) {
		this.ctx = ctx;
	}

	public void build() {
		varAssigned = getVars(ctx.exprlist()).iterator().next();
		inVars = getVars(ctx.testlist());
	}

	public String getVarAssigned() {
		return varAssigned;
	}

	public void setVarAssigned(String varAssigned) {
		this.varAssigned = varAssigned;
	}

	public Set<String> getInVars() {
		return inVars;
	}

	public void setInVars(Set<String> inVars) {
		this.inVars = inVars;
	}

	public String getText() {
		return "for" + ctx.exprlist().getText() + " in " + ctx.testlist().getText();
	}

	public String[] str() {
		return new String[] { "For expression: for " + ctx.exprlist().getText() + " in " + ctx.testlist().getText(),
				"Depends on vars: " + inVars.toString() };
	}

}

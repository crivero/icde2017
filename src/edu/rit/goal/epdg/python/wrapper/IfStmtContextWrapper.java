package edu.rit.goal.epdg.python.wrapper;

import java.util.Set;

import edu.rit.goal.epdg.python.antlr.Python3Parser.If_stmtContext;

public class IfStmtContextWrapper extends AbstractContextWrapper {

	{
		_EXCLUDE = new String[] { "\\d+", "\\*", "=", "\\(", "\\)", "&&", "||", "<", ">", "<=", ">=", "==", "%" };
	}
	
	private If_stmtContext ctx;
	
	private Set<String> conditionVars;
	private String conditionText;
	
	public IfStmtContextWrapper(If_stmtContext ctx) {
		this.ctx = ctx;
	}
	
	public void build() {
		conditionVars = getVars(ctx.test(0));
		conditionText = ctx.test(0).getText();
	}

	public Set<String> getConditionVars() {
		return conditionVars;
	}

	public void setConditionVars(Set<String> conditionVars) {
		this.conditionVars = conditionVars;
	}

	public String getConditionText() {
		return conditionText;
	}

	public void setConditionText(String conditionText) {
		this.conditionText = conditionText;
	}
	
	public String[] str() {
		return new String[]{"If condition expression: " + conditionText, "Condition vars: " + conditionVars.toString()};
	}
	
}

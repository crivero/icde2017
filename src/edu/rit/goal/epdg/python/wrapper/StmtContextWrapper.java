package edu.rit.goal.epdg.python.wrapper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.tree.ParseTree;

import edu.rit.goal.epdg.python.UnpackResult;
import edu.rit.goal.epdg.python.antlr.Python3Parser;
import edu.rit.goal.epdg.python.antlr.Python3Parser.AugassignContext;
import edu.rit.goal.epdg.python.antlr.Python3Parser.Expr_stmtContext;
import edu.rit.goal.epdg.python.antlr.Python3Parser.Testlist_star_exprContext;

public class StmtContextWrapper extends AbstractContextWrapper {

	public static final int ASSIGN = 0, AUG_ASSIGN = 1, FUNC_CALL = 2, UNKNOWN = 3;

	{
		_EXCLUDE = new String[] { "\\d+", "\\+", "-", "\\*", "/", "\\(", "\\)", "\\[", "\\]", "','",
				// Shouldn't be excluding print because it can be used as a
				// variable name.
				// TODO: Instead of excluding use regex that accounts for all
				// accepted var names.
				"print", "str" };
	}

	private Expr_stmtContext ctx;

	private String varAssigned;
	private Set<String> expVars;
	private List<UnpackResult> unpacked;

	public StmtContextWrapper(Expr_stmtContext ctx) {
		this.ctx = ctx;
	}

	public Python3Parser.Expr_stmtContext getCtx() {
		return ctx;
	}

	public void setCtx(Python3Parser.Expr_stmtContext ctx) {
		this.ctx = ctx;
	}

	public int exprStmt() {
		final List<Testlist_star_exprContext> tlseCtx = ctx.testlist_star_expr();
		final Testlist_star_exprContext leftHandSide = tlseCtx.get(0);
		final AugassignContext augAssignCtx = ctx.augassign();
		if (augAssignCtx != null) {
			varAssigned = leftHandSide.getText();
			expVars = getVars(ctx.testlist());
			return AUG_ASSIGN;
		}
		final ParseTree child = ctx.getChild(1);
		if (child != null && "=".equals(child.getText())) {
			varAssigned = leftHandSide.getText();
			expVars = getVars(tlseCtx.get(1));
			unpacked = unpack(leftHandSide, tlseCtx.get(1));
			return ASSIGN;
		}
		final int startTokenType = ctx.getStart().getType();
		// TODO: Check that '(' comes after to confirm is a function call.
		if (Python3Parser.NAME == startTokenType) {
			expVars = getVars(ctx);
			return FUNC_CALL;
		}
		return UNKNOWN;
	}

	public String getVarAssigned() {
		return varAssigned;
	}

	public void setVarAssigned(String varAssigned) {
		this.varAssigned = varAssigned;
	}

	public Set<String> getExpVars() {
		return expVars;
	}

	public void setExpVars(Set<String> expVars) {
		this.expVars = expVars;
	}

	public List<UnpackResult> getUnpacked() {
		return unpacked;
	}

	public void setUnpacked(List<UnpackResult> unpacked) {
		this.unpacked = unpacked;
	}

	private List<UnpackResult> unpack(Testlist_star_exprContext leftHandSide, Testlist_star_exprContext rightHandSide) {
		List<UnpackResult> result = new LinkedList<>();
		boolean isComma;
		String partialExpr;
		if (leftHandSide.getChildCount() < rightHandSide.getChildCount()) {
			System.err.println("Too many values to unpack.");
			throw new RuntimeException();
		}
		for (int i = 0; i < leftHandSide.getChildCount(); i++) {
			final ParseTree lhs = leftHandSide.getChild(i);
			isComma = ",".equals(lhs.getText());
			if (!isComma) {
				final ParseTree rhs = rightHandSide.getChild(i);
				if (rhs != null) {
					partialExpr = lhs.getText() + " = " + rhs.getText();
					result = matchIdentifiers(ctx.getText(), partialExpr, lhs.getText(), rhs.getText());
					for (final UnpackResult ur : result) {
						System.out.println(ur.toString());
					}
				} else {
					System.err.println("Potential not enough values to unpack (check that it is an iterable)");
				}
			}
		}
		return result;
	}

	private List<UnpackResult> matchIdentifiers(String fullExpr, String partialExpr, String depVars, String indepVars) {
		final List<UnpackResult> result = new LinkedList<>();
		final Pattern idRegex = Pattern.compile("([^\\d\\W]\\w*)", Pattern.UNICODE_CHARACTER_CLASS);
		final Matcher mDepVars = idRegex.matcher(depVars);
		Matcher mIndepVars;
		final List<String> exprs = new LinkedList<>(Arrays.asList(indepVars.split(",")));
		while (mDepVars.find()) {
			final Set<String> sIndepVars = new HashSet<>();
			final String nextExpr = exprs.remove(0);
			mIndepVars = idRegex.matcher(nextExpr);
			while (mIndepVars.find()) {
				sIndepVars.add(mIndepVars.group());
			}
			result.add(new UnpackResult(fullExpr, partialExpr, mDepVars.group(), sIndepVars));
		}
		if (exprs.size() != 0) {
			System.err.println("Too many values to unpack.");
			throw new RuntimeException();
		}
		return result;
	}
}

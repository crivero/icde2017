package edu.rit.goal.epdg.python.wrapper;

import java.util.Set;

import edu.rit.goal.epdg.python.antlr.Python3Parser.Return_stmtContext;

public class ReturnStmtContextWrapper extends AbstractContextWrapper {

	private Return_stmtContext ctx;

	private Set<String> depVars;

	public ReturnStmtContextWrapper(Return_stmtContext ctx) {
		this.ctx = ctx;
	}

	public void build() {
		depVars = getVars(ctx.testlist());
	}

	public Set<String> getDepVars() {
		return depVars;
	}

	public void setDepVars(Set<String> depVars) {
		this.depVars = depVars;
	}

}

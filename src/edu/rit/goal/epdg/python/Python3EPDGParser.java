package edu.rit.goal.epdg.python;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;

import edu.rit.goal.epdg.IEPDGParser;
import edu.rit.goal.epdg.object.Edge;
import edu.rit.goal.epdg.object.Vertex;
import edu.rit.goal.epdg.object.VertexType;
import edu.rit.goal.epdg.python.antlr.Python3Lexer;
import edu.rit.goal.epdg.python.antlr.Python3Parser;

public class Python3EPDGParser implements IEPDGParser {

	private final Python3BaseListener listener = new Python3BaseListener();
	private final Map<String, DirectedGraph<Vertex, Edge>> graphs = new HashMap<>();
	private final Map<String, Map<VertexType, List<Vertex>>> verticesByType = new HashMap<>();

	@Override
	public void parse(String submission, int errorLineOffset) {
		final CharStream submissionStream = new ANTLRInputStream(submission);
		final Lexer lexer = new Python3Lexer(submissionStream);
		final CommonTokenStream tokens = new CommonTokenStream(lexer);
		final Python3Parser parser = new Python3Parser(tokens);

		final ParseTree tree = parser.file_input();
		final ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(listener, tree);
		// Generate the graphs.
		for (final String method : listener.getMethods()) {
			final DirectedGraph<Vertex, Edge> graph = new DefaultDirectedGraph<>(Edge.class);
			graphs.put(method, graph);
			verticesByType.put(method, new HashMap<>());

			for (final Vertex current : listener.getVertices(method)) {
				graph.addVertex(current);

				List<Vertex> list = verticesByType.get(method).get(current.getType());
				if (list == null)
					verticesByType.get(method).put(current.getType(), list = new ArrayList<>());
				list.add(current);
			}
			for (final Edge current : listener.getEdges(method))
				graph.addEdge(current.getFromVertex(), current.getToVertex(), current);
		}
	}

	@Override
	public Set<String> getMethods() {
		return listener.getMethods();
	}

	public Map<String, DirectedGraph<Vertex, Edge>> getGraphs() {
		return graphs;
	}

	public Map<String, Map<VertexType, List<Vertex>>> getVerticesByType() {
		return verticesByType;
	}
}

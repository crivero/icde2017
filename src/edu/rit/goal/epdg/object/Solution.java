package edu.rit.goal.epdg.object;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {
	private Map<PatternVertex, Vertex> nodeMapping = new HashMap<>();
	private Map<String, String> varMapping = new HashMap<>();
	private List<PatternVertex> approxNodes = new ArrayList<>();
	
	public Solution clone() {
		Solution sol = new Solution();
		sol.nodeMapping = new HashMap<>(nodeMapping);
		sol.varMapping = new HashMap<>(varMapping);
		sol.approxNodes = new ArrayList<>(approxNodes);
		return sol;
	}
	public Map<PatternVertex, Vertex> getNodeMapping() {
		return nodeMapping;
	}
	public Map<String, String> getVarMapping() {
		return varMapping;
	}
	public List<PatternVertex> getApproxNodes() {
		return approxNodes;
	}
}

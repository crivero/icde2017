package edu.rit.goal.ws;

import edu.rit.goal.epdg.python.Python3EPDGParser;

public class Python3MatchingService extends MatchingService {

	public Python3MatchingService() {
		super(new Python3EPDGParser());
	}

	@Override
	public int getErrorLineOffset() {
		return 0;
	}
}

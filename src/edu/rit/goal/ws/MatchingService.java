package edu.rit.goal.ws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Sets;

import edu.rit.goal.epdg.IEPDGParser;
import edu.rit.goal.epdg.object.Constraint;
import edu.rit.goal.epdg.object.Feedback;
import edu.rit.goal.epdg.object.Feedback.FeedbackType;
import edu.rit.goal.epdg.object.MatchingResult;
import edu.rit.goal.epdg.object.Pattern;
import edu.rit.goal.epdg.object.Pattern.PatternType;
import edu.rit.goal.epdg.object.Solution;
import edu.rit.goal.epdg.object.VertexType;

public abstract class MatchingService implements IMatchingService {

	private final IEPDGParser epdgParser;

	public MatchingService (IEPDGParser epdgParser) {
		this.epdgParser = epdgParser;
	}

	@Override
	public Map<String, Map<Feedback, List<Feedback>>> match(String submission, Map<String, String> patternsToApply, Map<String, String> constraintsToApply, int limit) {
		final List<String> errors = new ArrayList<>();
		Map<String, Map<Feedback, List<Feedback>>> feedback = new HashMap<>();
		final Map<String, String> submissionMethodsMapping = new HashMap<>();

		// Parse the submission.
		try {
			epdgParser.parse(submission, getErrorLineOffset());
		} catch (final Exception oops) {
			errors.add("when parsing the submission");
		}

		// Parse the patterns.
		final Map<String, Map<PatternType, Integer>> patterns = new HashMap<>();
		try {
			for (final String method : patternsToApply.keySet()) {
				final String[] selected = patternsToApply.get(method).split(",");

				final Map<PatternType, Integer> typesAndTimes = new HashMap<>();
				patterns.put(method, typesAndTimes);

				for (final String currentPattern : selected) {
					final String[] typeAndNumber = currentPattern.split("--");

					final PatternType type = PatternType.valueOf(typeAndNumber[0]);
					final int times = Integer.valueOf(typeAndNumber[1]);

					typesAndTimes.put(type, times);
				}
			}
		} catch (final Exception oops) {
			errors.add("Something went wrong when parsing the selected patterns.");
		}

		// Parse the constraints.
		final Map<String, List<Constraint>> constraints = new HashMap<>();
		try {
			for (final String method : constraintsToApply.keySet()) {
				final String[] selected = constraintsToApply.get(method).split("\\|\\|\\|\\|\\|\\|");
				constraints.put(method, new ArrayList<>());

				for (final String currentConstraint : selected) {
					final Constraint c = Constraint.parse(currentConstraint);
					if (c != null)
						constraints.get(method).add(c);
				}
			}
		} catch (final Exception oops) {
			errors.add("Something went wrong when parsing the selected constraints.");
		}

		if (errors.isEmpty()) {
			final Set<String> submissionMethods = epdgParser.getMethods();
			final Set<String> expectedMethods = patternsToApply.keySet();

			matchSubmissions(0, new ArrayList<>(expectedMethods), submissionMethods, new HashMap<>(), patterns, constraints, epdgParser, feedback, submissionMethodsMapping);

			// Change expected methods to the matched ones.
			final Map<String, Map<Feedback, List<Feedback>>> tempFeedback = new HashMap<>();
			for (final String expected : feedback.keySet())
				tempFeedback.put(submissionMethodsMapping.get(expected), feedback.get(expected));
			feedback = tempFeedback;
		} else
			for (final String err : errors)
				System.err.println(err);

		return feedback;
	}

	private void matchSubmissions(int i, List<String> expectedMethods, Set<String> submissionMethods, Map<String, String> current,
			Map<String, Map<PatternType, Integer>> patterns, Map<String, List<Constraint>> constraints, IEPDGParser parser,
			Map<String, Map<Feedback, List<Feedback>>> feedback, Map<String, String> mappingResult) {
		if (i == expectedMethods.size()) {
			final Map<String, Map<Feedback, List<Feedback>>> currentFeedback = new HashMap<>();
			final Map<String, Map<PatternType, List<Solution>>> currentSolutions = new HashMap<>();

			// Match patterns.
			for (final String expected : current.keySet()) {
				final String fromSubmission = current.get(expected);
				currentFeedback.put(expected, new HashMap<>());
				currentSolutions.put(expected, new HashMap<>());

				for (final PatternType type : patterns.get(expected).keySet()) {
					final int times = patterns.get(expected).get(type);

					final MatchingResult result = Pattern.getInstance().getPattern(type).match(parser.getGraphs().get(fromSubmission), parser.getVerticesByType().get(fromSubmission), times);

					currentFeedback.get(expected).putAll(result.getFeedback());
					currentSolutions.get(expected).put(type, result.getSolutions());
				}
			}

			// Match constraints.
			for (final String expected : current.keySet())
				if (constraints.get(expected) != null)
					for (final Constraint c : constraints.get(expected)) {
						// Get all declarations.
						c.setParameters(parser.getVerticesByType().get(current.get(expected)).get(VertexType.DECL));
						c.setSolutions(currentSolutions.get(expected));
						c.setFunctionMapping(current);
						c.setGraph(parser.getGraphs().get(current.get(expected)));

						// f will be null when we are not able to check this constraint.
						for (final Feedback f : c.match())
							currentFeedback.get(expected).put(f, new ArrayList<>());
					}

			// Compare feedback.
			final double feedbackScore = computeScore(feedback);
			final double currentFeedbackScore = computeScore(currentFeedback);

			if (feedback.isEmpty() || currentFeedbackScore > feedbackScore) {
				feedback.clear();
				feedback.putAll(currentFeedback);

				mappingResult.clear();
				mappingResult.putAll(current);
			}
		} else {
			final String currentExpectedMethod = expectedMethods.get(i);
			// Remove those that are already mapped.
			for (final String currentSubmissionMethod : Sets.difference(submissionMethods, new HashSet<>(current.values()))) {
				current.put(currentExpectedMethod, currentSubmissionMethod);
				matchSubmissions(i + 1, expectedMethods, submissionMethods, current, patterns, constraints, parser, feedback, mappingResult);
				current.remove(currentExpectedMethod);
			}
		}
	}

	private double computeScore(Map<String, Map<Feedback, List<Feedback>>> feedback) {
		double ret = 0.0;

		for (final String f : feedback.keySet())
			for (final Feedback feed : feedback.get(f).keySet()) {
				if (feed.getType().equals(FeedbackType.Correct))
					ret+=1.0;
				else if (feed.getType().equals(FeedbackType.Almost))
					ret+=0.5;
			}

		return ret;
	}

}

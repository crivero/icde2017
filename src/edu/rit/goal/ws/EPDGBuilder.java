package edu.rit.goal.ws;

import java.util.Map;

import org.jgrapht.DirectedGraph;

import edu.rit.goal.epdg.java.Java8EPDGParser;
import edu.rit.goal.epdg.object.Edge;
import edu.rit.goal.epdg.object.Vertex;
import edu.rit.goal.epdg.python.Python3EPDGParser;
import edu.rit.goal.ws.ParsingResult.Language;

public class EPDGBuilder {

	private Java8EPDGParser javaParser;
	private Python3EPDGParser pythonParser;

	public EPDGBuilder() {
		javaParser = new Java8EPDGParser();
		pythonParser = new Python3EPDGParser();
	}

	public ParsingResult fromCode(String code) {
		ParsingResult result = new ParsingResult();
		Map<String, DirectedGraph<Vertex, Edge>> graphs;
		if ((code.contains("def ") && code.contains(":"))
				|| (!code.contains("{") && !code.contains("}") && !code.contains(";"))) {
			pythonParser.parse(code, 0);
			result = new ParsingResult(Language.PYTHON, pythonParser.getGraphs());
		} else {
			// For Java, we need to wrap the code with a class and a method.
			String wrapped = wrapSubmission(code);
			javaParser.parse(wrapped, 2);
			result = new ParsingResult(Language.JAVA, javaParser.getGraphs());
		}

		return result;
	}

	private String wrapSubmission(String submission) {
		StringBuilder sb = new StringBuilder("public class Submission {\n");
		sb.append(submission);
		sb.append("}");
		return sb.toString();
	}

}

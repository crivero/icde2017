package edu.rit.goal.ws;

public class MatchingServiceFactory {

	public static IMatchingService create(String extension) {
		IMatchingService result = null;
		switch (extension) {
		case "py":
			result = new Python3MatchingService();
			break;
		case "java":
			result = new Java8MatchingService();
			break;
		default:
			System.err.println("The file extension ." + extension + " is not supported.");
			break;
		}
		return result;
	}

}

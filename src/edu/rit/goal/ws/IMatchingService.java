package edu.rit.goal.ws;

import java.util.List;
import java.util.Map;

import edu.rit.goal.epdg.object.Feedback;

public interface IMatchingService {

	Map<String, Map<Feedback, List<Feedback>>> match(String submission, Map<String, String> patternsToApply, Map<String, String> constraintsToApply, int limit);

	int getErrorLineOffset();
}

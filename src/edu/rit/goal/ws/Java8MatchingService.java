package edu.rit.goal.ws;

import java.util.List;
import java.util.Map;

import edu.rit.goal.epdg.java.Java8EPDGParser;
import edu.rit.goal.epdg.object.Feedback;

public class Java8MatchingService extends MatchingService {

	public Java8MatchingService() {
		super(new Java8EPDGParser());
	}

	@Override
	public Map<String, Map<Feedback, List<Feedback>>> match(String submission, Map<String, String> patternsToApply,
			Map<String, String> constraintsToApply, int limit) {
		// We need to wrap the submission with a class and a method.
		final String wrappedSubmission = wrapSubmission(submission);
		return super.match(wrappedSubmission, patternsToApply, constraintsToApply, limit);
	}

	private String wrapSubmission(String submission) {
		final StringBuilder sb = new StringBuilder("public class Submission {\n");
		sb.append(submission);
		sb.append("}");
		return sb.toString();
	}

	@Override
	public int getErrorLineOffset() {
		return 2;
	}

}
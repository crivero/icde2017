package edu.rit.goal.test;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import edu.rit.goal.epdg.object.Feedback;
import edu.rit.goal.ws.IMatchingService;
import edu.rit.goal.ws.MatchingServiceFactory;

public class MatchSubmission {
	public static void main(String[] args) throws Exception {
		final int assignment = 1, solNumber = 2;
		final String extension = "py";
		final Map<String, Map<Feedback, List<Feedback>>> result = getFeedback(extension, assignment, solNumber);

		for (final String method : result.keySet()) {
			System.out.println("Method: " + method);

			for (final Feedback major : result.get(method).keySet()) {
				System.out.println("\t" + major.getType() + "--" + major.getText());

				for (final Feedback minor : result.get(method).get(major))
					System.out.println("\t\t" + minor.getType() + "--" + minor.getText());
			}
		}
	}

	public static Map<String, Map<Feedback, List<Feedback>>> getFeedback(String extension, int assignment, int solution)
			throws Exception {
		final String submissionPath = CheckUtils.getSubmissionPath(extension, assignment, solution);
		final Map<String, String> patternsToApply = CheckUtils.getAssignmentPatterns(assignment),
				constraintsToApply = CheckUtils.getAssignmentConstraints(assignment);
		final Scanner scan = new Scanner(new File(submissionPath));
		final StringBuffer submission = new StringBuffer();
		while (scan.hasNext()) {
			submission.append(scan.nextLine());
			submission.append("\n");
		}
		scan.close();
		final IMatchingService service = MatchingServiceFactory.create(extension);
		return service.match(submission.toString(), patternsToApply, constraintsToApply, Integer.MAX_VALUE);
	}

}

package edu.rit.goal.test;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import edu.rit.goal.epdg.object.ConstraintFeedback;
import edu.rit.goal.epdg.object.Feedback;
import edu.rit.goal.epdg.object.Feedback.FeedbackType;
import edu.rit.goal.epdg.object.PatternFeedback;

public class CheckUtils {

	public static void checkJava(int assignment, int solution, String correct) throws Exception {
		check("java", assignment, solution, correct);
	}

	public static void checkPython(int assignment, int solution, String correct) throws Exception {
		check("py", assignment, solution, correct);
	}

	public static String getSubmissionPath(String extension, int assignment, int solution) {
		String res = "../icde2017-submissions/assignments/" + getAssignmentFolder(assignment) + "/old";
		final NumberFormat nf = NumberFormat.getNumberInstance();
		((DecimalFormat) nf).applyPattern("00");
		switch (extension) {
		case "java":
			res += "/java/sol" + nf.format(solution) + ".java";
			break;
		case "py":
			res += "/python/sol" + nf.format(solution) + ".py";
			break;
		}
		return res;
	}

	public static void check(String extension, int assignment, int solution, String correct) throws Exception {
		final Map<String, Map<Feedback, List<Feedback>>> result = MatchSubmission.getFeedback(extension, assignment,
				solution);
		final String[] arrayOfMethods = correct.split("\\|");

		for (final String method : arrayOfMethods) {
			final String[] array = method.split("--");
			final String m = array[0];

			for (String typeOrName : array[1].split(",")) {
				final boolean isAlmost = typeOrName.startsWith("~");
				typeOrName = typeOrName.replace("~", "");

				Feedback found = null;

				for (final Iterator<Feedback> it = result.get(m).keySet().iterator(); found == null && it.hasNext();) {
					final Feedback f = it.next();

					PatternFeedback p = null;
					ConstraintFeedback c = null;

					if (f instanceof PatternFeedback)
						p = (PatternFeedback) f;
					else if (f instanceof ConstraintFeedback)
						c = (ConstraintFeedback) f;

					if (p != null && p.getPatternType().toString().equals(typeOrName) && (isAlmost
							? f.getType().equals(FeedbackType.Almost) : f.getType().equals(FeedbackType.Correct)))
						found = f;
					else if (c != null && c.getConstraint().toString().equals(typeOrName) && (isAlmost
							? f.getType().equals(FeedbackType.Almost) : f.getType().equals(FeedbackType.Correct)))
						found = f;
				}

				if (found == null)
					System.err.println("Assignment: " + assignment + "; Solution: " + solution + "; Method: " + m
							+ "; Error: " + typeOrName);
				else
					result.get(m).remove(found);
			}

			// The rest must be incorrect.
			for (final Feedback f : result.get(m).keySet())
				if (!f.getType().equals(FeedbackType.Incorrect)) {
					PatternFeedback p = null;
					ConstraintFeedback c = null;
					String type = null;

					if (f instanceof PatternFeedback) {
						p = (PatternFeedback) f;
						type = p.getPatternType().toString();
					} else if (f instanceof ConstraintFeedback) {
						c = (ConstraintFeedback) f;
						type = c.getConstraint();
					}

					System.err.println("Assignment: " + assignment + "; Solution: " + solution + "; Method: " + m
							+ "; Error: " + type + " is correct!");
				}
		}
	}

	public static String getAssignmentFolder(int a) {
		String folder = "";

		if (a == 1)
			folder = "running";
		if (a == 2)
			folder = "esc-LAB-3-P1-V1";
		if (a == 3)
			folder = "esc-LAB-3-P2-V1";
		if (a == 4)
			folder = "esc-LAB-3-P2-V2";
		if (a == 5)
			folder = "esc-LAB-3-P3-V1";
		if (a == 6)
			folder = "esc-LAB-3-P3-V2";
		if (a == 7)
			folder = "esc-LAB-3-P4-V1";
		if (a == 8)
			folder = "esc-LAB-3-P4-V2";
		if (a == 9)
			folder = "mitx-derivatives";
		if (a == 10)
			folder = "mitx-polynomials";
		if (a == 11)
			folder = "rit-all-g-medals";
		if (a == 12)
			folder = "rit-medals-by-ath";

		return folder;
	}

	public static Map<String, String> getAssignmentPatterns(int a) {
		final Map<String, String> patternsToApply = new HashMap<>();

		if (a == 1)
			patternsToApply.put("main",
					"ACCESS_EVEN--" + 1 + "," + "ACCESS_ODD--" + 1 + "," + "COND_CUMULATIVELY_ADD--" + 1 + ","
							+ "COND_CUMULATIVELY_MULT--" + 1 + "," + "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + ","
							+ "PRINT_CONSOLE--" + 2);

		if (a == 2) {
			patternsToApply.put("main", "FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL--" + 1 + ","
					+ "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + "," + "PRINT_CONSOLE--" + 1);

			patternsToApply.put("factorial", "ONE_LIMIT_INCLUSIVE_LOOP--" + 1 + "," + "CUMULATIVELY_MULT--" + 1 + ","
					+ "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + "," + "RETURN--" + 1);
		}

		if (a == 3) {
			patternsToApply.put("main", "FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL--" + 1 + ","
					+ "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + "," + "PRINT_CONSOLE--" + 1);

			patternsToApply.put("fibonacci",
					"ONE_LIMIT_INCLUSIVE_LOOP--" + 1 + "," + "CUMULATIVELY_ADD--" + 1 + "," + "RETURN--" + 1 + ","
							+ "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + "," + "REPEATEDLY_SUBSTRACT_FROM_VARIABLE--" + 1);
		}

		if (a == 4)
			patternsToApply.put("main", "CUMULATIVELY_ADD--" + 1 + "," + "RETURN--" + 1 + ","
					+ "DECIMAL_DIGIT_EXTRACT--" + 1 + "," + "COPY_VAR--" + 1);

		if (a == 5)
			patternsToApply.put("main",
					"CUMULATIVELY_ADD--" + 1 + "," + "RETURN--" + 1 + "," + "DECIMAL_DIGIT_EXTRACT--" + 1 + ","
							+ "COPY_VAR--" + 1 + "," + "ONE_LIMIT_INCLUSIVE_LOOP--" + 1 + ","
							+ "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + "," + "NUMBER_OF_DECIMAL_DIGITS--" + 1);

		if (a == 6) {
			patternsToApply.put("main",
					"FIND_NUMBER_POSITIVE_STATIC_INTERVAL--" + 1 + "," + "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + ","
							+ "COND_CUMULATIVELY_ADD--" + 1 + "," + "PRINT_CONSOLE--" + 1);

			patternsToApply.put("factorial", "ONE_LIMIT_INCLUSIVE_LOOP--" + 1 + "," + "CUMULATIVELY_MULT--" + 1 + ","
					+ "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + "," + "RETURN--" + 1);
		}

		if (a == 7)
			patternsToApply.put("main",
					"CUMULATIVELY_ADD--" + 1 + "," + "RETURN--" + 1 + "," + "DECIMAL_DIGIT_EXTRACT--" + 1 + ","
							+ "COPY_VAR--" + 1 + "," + "ONE_LIMIT_INCLUSIVE_LOOP--" + 1 + ","
							+ "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + "," + "NUMBER_OF_DECIMAL_DIGITS--" + 1);

		if (a == 8) {
			patternsToApply.put("main",
					"FIND_NUMBER_POSITIVE_STATIC_INTERVAL--" + 1 + "," + "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + ","
							+ "COND_CUMULATIVELY_ADD--" + 1 + "," + "PRINT_CONSOLE--" + 1);

			patternsToApply.put("fibonacci",
					"ONE_LIMIT_INCLUSIVE_LOOP--" + 1 + "," + "CUMULATIVELY_ADD--" + 1 + "," + "RETURN--" + 1 + ","
							+ "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + "," + "REPEATEDLY_SUBSTRACT_FROM_VARIABLE--" + 1);
		}

		if (a == 9)
			patternsToApply.put("main", "ARRAY_COMPUTATION--" + 1 + "," + "CONDITIONAL_PRINT_CONSTANT--" + 2 + ","
					+ "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + ",");

		if (a == 10)
			patternsToApply.put("main", "ARRAY_COMPUTATION--" + 1 + "," + "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + ","
					+ "CUMULATIVELY_ADD--" + 1 + "," + "PRINT_CONSOLE--" + 1 + ",");

		if (a == 11)
			patternsToApply.put("main",
					"ACCESS_POS_0_OUT_5_FILE--" + 1 + "," + "ACCESS_POS_1_OUT_5_FILE--" + 1 + ","
							+ "ACCESS_POS_2_OUT_5_FILE--" + 1 + "," + "ACCESS_POS_3_OUT_5_FILE--" + 1 + ","
							+ "ACCESS_POS_4_OUT_5_FILE--" + 1 + "," + "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + ","
							+ "COND_CUMULATIVELY_ADD--" + 1 + "," + "PRINT_CONSOLE--" + 1 + ","
							+ "NO_TWO_ASSIGNED_VARIABLES_LOOP--" + 0 + ",");

		if (a == 12)
			patternsToApply.put("main",
					"ACCESS_POS_0_OUT_5_FILE--" + 1 + "," + "ACCESS_POS_1_OUT_5_FILE--" + 1 + ","
							+ "ACCESS_POS_2_OUT_5_FILE--" + 1 + "," + "ACCESS_POS_3_OUT_5_FILE--" + 1 + ","
							+ "ACCESS_POS_4_OUT_5_FILE--" + 1 + "," + "TWO_ASSIGNMENTS_LOOP_INDEX--" + 0 + ","
							+ "COND_CUMULATIVELY_ADD--" + 3 + "," + "PRINT_CONSOLE--" + 3 + ","
							+ "NO_TWO_ASSIGNED_VARIABLES_LOOP--" + 0 + ",");

		return patternsToApply;
	}

	public static Map<String, String> getAssignmentConstraints(int a) {
		final Map<String, String> constraintsToApply = new HashMap<>();

		if (a == 1)
			constraintsToApply.put("main",
					"C1 ACCESS_EVEN.6<=>COND_CUMULATIVELY_MULT.2|||Variable :c[] is[ not] accessing even positions in :s[array]||||||"
							+ "C2 ACCESS_ODD.6<=>COND_CUMULATIVELY_ADD.2|||Variable :c[] is[ not] accessing odd positions in :s[array]||||||"
							+ "C3 PRINT_CONSOLE.1<=>COND_CUMULATIVELY_ADD.2|||Variable :x[] is[ not] cumulatively added and printed to console||||||"
							+ "C4 PRINT_CONSOLE.1<=>COND_CUMULATIVELY_MULT.2|||Variable :x[] is[ not] cumulatively multiplied and printed to console||||||");

		if (a == 2) {
			constraintsToApply.put("main",
					"C1 FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL.2<=>PRINT_CONSOLE.1|||Variable :x[] is[ not] printed to console||||||"
							+ "C2 FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL.4>>>.*:methodParameter|||The limit of the loop is[ not] :methodParameter[the parameter]||||||"
							+ "C3 FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL.5>>>:low=factorial\\(:x\\)|||Variable :low[low limit] is[ not] computed by calling factorial(:x[parameter])||||||"
							+ "C4 FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL.6>>>:high=factorial\\(:x\\+1\\)|||Variable :high[high limit] is[ not] computed by calling factorial(:x[parameter] + 1)||||||"
							+ "C5 FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL.7>>>:low\\<=:methodParameter\\&\\&:methodParameter\\<:high|||Parameter :methodParameter[] is[ not] between :low[low limit] and :high[high limit]||||||");

			constraintsToApply.put("factorial",
					"C1 ONE_LIMIT_INCLUSIVE_LOOP.3>>>.*:methodParameter|||The limit of the loop is[ not] :methodParameter[the parameter]||||||"
							+ "C2 CUMULATIVELY_MULT.3<=>ONE_LIMIT_INCLUSIVE_LOOP.3|||Variable :c[] is[ not] cumulatively multiplied from one to :x[limit]||||||"
							+ "C3 CUMULATIVELY_MULT.2>>>:c\\*=ONE_LIMIT_INCLUSIVE_LOOP.:x|||Variable ONE_LIMIT_INCLUSIVE_LOOP.:x[] is[ not] used to cumulatively multiplied :c[another variable]||||||"
							+ "C4 RETURN.1<=>CUMULATIVELY_MULT.2|||Variable :c[] is[ not] cumulatively multiplied and returned||||||");
		}

		if (a == 3) {
			constraintsToApply.put("main",
					"C1 FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL.2<=>PRINT_CONSOLE.1|||Variable :x[] is[ not] printed to console||||||"
							+ "C2 FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL.4>>>.*:methodParameter|||The limit of the loop is[ not] :methodParameter[the parameter]||||||"
							+ "C3 FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL.5>>>:low=fibonacci\\(:x\\)|||Variable :low[low limit] is[ not] computed by calling fibonacci(:x[variable])||||||"
							+ "C4 FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL.6>>>:high=fibonacci\\(:x\\+1\\)|||Variable :high[high limit] is[ not] computed by calling fibonacci(:x[variable] + 1)||||||"
							+ "C5 FIND_NUMBER_POSITIVE_DYNAMIC_INTERVAL.7>>>:low\\<=:methodParameter\\&\\&:methodParameter\\<:high|||Parameter :methodParameter[] is[ not] between :low[low limit] and :high[high limit]||||||");

			constraintsToApply.put("fibonacci",
					"C1 ONE_LIMIT_INCLUSIVE_LOOP.3>>>.*:methodParameter|||The limit of the loop is[ not] :methodParameter[the parameter]||||||"
							+ "C2 CUMULATIVELY_ADD.3<=>ONE_LIMIT_INCLUSIVE_LOOP.3|||Variable :c[] is[ not] cumulatively added from one to :x[limit]||||||"
							+ "C3 REPEATEDLY_SUBSTRACT_FROM_VARIABLE.1>>>:x=1|||Variable :x[] is[ not] starting in one||||||"
							+ "C4 REPEATEDLY_SUBSTRACT_FROM_VARIABLE.2>>>:x=:y-:x|||Variable :x[] is[ not] re-assigned||||||"
							+ "C5 REPEATEDLY_SUBSTRACT_FROM_VARIABLE.2>>>:x=CUMULATIVELY_ADD.:c-:x|||Variable CUMULATIVELY_ADD.:c[] is[ not] substracted from :x[another variable]||||||"
							+ "C6 CUMULATIVELY_ADD.2>>>:c\\+=REPEATEDLY_SUBSTRACT_FROM_VARIABLE.:x|||Variable REPEATEDLY_SUBSTRACT_FROM_VARIABLE.:x[] is[ not] used to cumulatively add :c[another variable]||||||"
							+ "C7 RETURN.1<=>CUMULATIVELY_ADD.2|||Variable :c[] is[ not] cumulatively added and returned||||||"
							+ "C8 CUMULATIVELY_ADD.2-(DATA)->REPEATEDLY_SUBSTRACT_FROM_VARIABLE.2|||Variable REPEATEDLY_SUBSTRACT_FROM_VARIABLE.:y[] is[ not] substracted before cumulatively added||||||");
		}

		if (a == 4)
			constraintsToApply.put("main",
					"C1 CUMULATIVELY_ADD.3>>>COPY_VAR.:x>0|||Variable COPY_VAR.:x[] is[ not] used to control the loop when cumulatively adding :c[another variable]||||||"
							+ "C2 COPY_VAR.1>>>:x=:methodParameter|||Variable :x[] is[ not] initialized to :methodParameter[another variable]||||||"
							+ "C3 CUMULATIVELY_ADD.2>>>:c\\+=DECIMAL_DIGIT_EXTRACT.:d\\*DECIMAL_DIGIT_EXTRACT.:d\\*DECIMAL_DIGIT_EXTRACT.:d|||Variable :c[] is[ not] cumulatively added using DECIMAL_DIGIT_EXTRACT.:d[another variable] cubic||||||"
							+ "C4 RETURN.2>>>CUMULATIVELY_ADD.:c==:methodParameter|||You are[ not] returning CUMULATIVELY_ADD.:c[another variable]==:methodParameter[another variable]||||||"
							+ "C5 DECIMAL_DIGIT_EXTRACT.2-(DATA)->CUMULATIVELY_ADD.2|||Variable CUMULATIVELY_ADD.:c[] is[ not] cumulatively added to DECIMAL_DIGIT_EXTRACT.:d[another variable] modulo 10||||||");

		if (a == 5)
			constraintsToApply.put("main",
					"C1 NUMBER_OF_DECIMAL_DIGITS.1>>>(.*):methodParameter(.*)|||Variable NUMBER_OF_DECIMAL_DIGITS.:x[] is[ not] computed using :methodParameter[another variable]||||||"
							+ "C2 COPY_VAR.1>>>:x=:methodParameter|||Variable :x[] is[ not] initialized to :methodParameter[another variable]||||||"
							+ "C3 CUMULATIVELY_ADD.2>>>:c\\+=DECIMAL_DIGIT_EXTRACT.:d\\*Math\\.pow\\(10,NUMBER_OF_DECIMAL_DIGITS.:x\\-ONE_LIMIT_INCLUSIVE_LOOP.:x\\)|||Variable :c[] is[ not] cumulatively added using DECIMAL_DIGIT_EXTRACT.:d[another variable] times 10^(NUMBER_OF_DECIMAL_DIGITS.:x[x] - ONE_LIMIT_INCLUSIVE_LOOP.:x[y])||||||"
							+ "C4 RETURN.2>>>:methodParameter-CUMULATIVELY_ADD.:c|||You are[ not] returning :methodParameter[another variable]-CUMULATIVELY_ADD.:c[another variable]||||||"
							+ "C5 DECIMAL_DIGIT_EXTRACT.2-(DATA)->CUMULATIVELY_ADD.2|||Variable CUMULATIVELY_ADD.:c[] is[ not] cumulatively added to DECIMAL_DIGIT_EXTRACT.:d[another variable] modulo 10||||||"
							+ "C6 ONE_LIMIT_INCLUSIVE_LOOP.3>>>(.*)NUMBER_OF_DECIMAL_DIGITS.:x(.*)|||The loop does[ not] go until NUMBER_OF_DECIMAL_DIGITS.:x[another variable]||||||");

		if (a == 6) {
			constraintsToApply.put("main",
					"C1 FIND_NUMBER_POSITIVE_STATIC_INTERVAL.8<=>COND_CUMULATIVELY_ADD.2|||Variable :c[] is[ not] cumulatively added when :x[] is in the interval||||||"
							+ "C2 FIND_NUMBER_POSITIVE_STATIC_INTERVAL.5>>>:methodParameter|||The limit of the loop is[ not] :methodParameter[a parameter]||||||"
							+ "C3 FIND_NUMBER_POSITIVE_STATIC_INTERVAL.6>>>:methodParameter|||The limit of the loop is[ not] :methodParameter[a parameter]||||||"
							+ "C4 FIND_NUMBER_POSITIVE_STATIC_INTERVAL.10>>>:y=factorial\\(:x\\)|||Variable :y[] is[ not] computed by calling factorial(:x[])||||||"
							+ "C5 COND_CUMULATIVELY_ADD.2>>>:c\\+=1|||Variable :c[] is[ not] cumulatively added + 1||||||"
							+ "C6 PRINT_CONSOLE.1<=>COND_CUMULATIVELY_ADD.2|||Variable :x[] is[ not] cumulatively added and printed to console||||||");

			constraintsToApply.put("factorial",
					"C1 ONE_LIMIT_INCLUSIVE_LOOP.3>>>.*:methodParameter|||The limit of the loop is[ not] :methodParameter[the parameter]||||||"
							+ "C2 CUMULATIVELY_MULT.3<=>ONE_LIMIT_INCLUSIVE_LOOP.3|||Variable :c[] is[ not] cumulatively multiplied from one to :x[limit]||||||"
							+ "C3 CUMULATIVELY_MULT.2>>>:c\\*=ONE_LIMIT_INCLUSIVE_LOOP.:x|||Variable ONE_LIMIT_INCLUSIVE_LOOP.:x[] is[ not] used to cumulatively multiplied :c[another variable]||||||"
							+ "C4 RETURN.1<=>CUMULATIVELY_MULT.2|||Variable :c[] is[ not] cumulatively multiplied and returned||||||");
		}

		if (a == 7)
			constraintsToApply.put("main",
					"C1 NUMBER_OF_DECIMAL_DIGITS.1>>>(.*):methodParameter(.*)|||Variable NUMBER_OF_DECIMAL_DIGITS.:x[] is[ not] computed using :methodParameter[another variable]||||||"
							+ "C2 COPY_VAR.1>>>:x=:methodParameter|||Variable :x[] is[ not] initialized to :methodParameter[another variable]||||||"
							+ "C3 CUMULATIVELY_ADD.2>>>:c\\+=DECIMAL_DIGIT_EXTRACT.:d\\*Math\\.pow\\(10,NUMBER_OF_DECIMAL_DIGITS.:x\\-ONE_LIMIT_INCLUSIVE_LOOP.:x\\)|||Variable :c[] is[ not] cumulatively added using DECIMAL_DIGIT_EXTRACT.:d[another variable] times 10^(NUMBER_OF_DECIMAL_DIGITS.:x[x] - ONE_LIMIT_INCLUSIVE_LOOP.:x[y])||||||"
							+ "C4 RETURN.2>>>:methodParameter==CUMULATIVELY_ADD.:c|||You are[ not] returning :methodParameter[another variable]==CUMULATIVELY_ADD.:c[another variable]||||||"
							+ "C5 DECIMAL_DIGIT_EXTRACT.2-(DATA)->CUMULATIVELY_ADD.2|||Variable CUMULATIVELY_ADD.:c[] is[ not] cumulatively added to DECIMAL_DIGIT_EXTRACT.:d[another variable] modulo 10||||||"
							+ "C6 ONE_LIMIT_INCLUSIVE_LOOP.3>>>(.*)NUMBER_OF_DECIMAL_DIGITS.:x(.*)|||The loop does[ not] go until NUMBER_OF_DECIMAL_DIGITS.:x[another variable]||||||");

		if (a == 8) {
			constraintsToApply.put("main",
					"C1 FIND_NUMBER_POSITIVE_STATIC_INTERVAL.8<=>COND_CUMULATIVELY_ADD.2|||Variable :c[] is[ not] cumulatively added when :x[] is in the interval||||||"
							+ "C2 FIND_NUMBER_POSITIVE_STATIC_INTERVAL.5>>>:methodParameter|||The limit of the loop is[ not] :methodParameter[a parameter]||||||"
							+ "C3 FIND_NUMBER_POSITIVE_STATIC_INTERVAL.6>>>:methodParameter|||The limit of the loop is[ not] :methodParameter[a parameter]||||||"
							+ "C4 FIND_NUMBER_POSITIVE_STATIC_INTERVAL.10>>>:y=fibonacci\\(:x\\)|||Variable :y[] is[ not] computed by calling fibonacci(:x[])||||||"
							+ "C5 COND_CUMULATIVELY_ADD.2>>>:c\\+=1|||Variable :c[] is[ not] cumulatively added + 1||||||"
							+ "C6 PRINT_CONSOLE.1<=>COND_CUMULATIVELY_ADD.2|||Variable :x[] is[ not] cumulatively added and printed to console||||||");

			constraintsToApply.put("fibonacci",
					"C1 ONE_LIMIT_INCLUSIVE_LOOP.3>>>.*:methodParameter|||The limit of the loop is[ not] :methodParameter[the parameter]||||||"
							+ "C2 CUMULATIVELY_ADD.3<=>ONE_LIMIT_INCLUSIVE_LOOP.3|||Variable :c[] is[ not] cumulatively added from one to :x[limit]||||||"
							+ "C3 REPEATEDLY_SUBSTRACT_FROM_VARIABLE.1>>>:x=1|||Variable :x[] is[ not] starting in one||||||"
							+ "C4 REPEATEDLY_SUBSTRACT_FROM_VARIABLE.2>>>:x=:y-:x|||Variable :x[] is[ not] re-assigned||||||"
							+ "C5 REPEATEDLY_SUBSTRACT_FROM_VARIABLE.2>>>:x=CUMULATIVELY_ADD.:c-:x|||Variable CUMULATIVELY_ADD.:c[] is[ not] substracted from :x[another variable]||||||"
							+ "C6 CUMULATIVELY_ADD.2>>>:c\\+=REPEATEDLY_SUBSTRACT_FROM_VARIABLE.:x|||Variable REPEATEDLY_SUBSTRACT_FROM_VARIABLE.:x[] is[ not] used to cumulatively add :c[another variable]||||||"
							+ "C7 RETURN.1<=>CUMULATIVELY_ADD.2|||Variable :c[] is[ not] cumulatively added and returned||||||"
							+ "C8 CUMULATIVELY_ADD.2-(DATA)->REPEATEDLY_SUBSTRACT_FROM_VARIABLE.2|||Variable REPEATEDLY_SUBSTRACT_FROM_VARIABLE.:y[] is[ not] substracted before cumulatively added||||||");
		}

		if (a == 9)
			constraintsToApply.put("main",
					"C1 ARRAY_COMPUTATION.6>>>System\\.out\\.print(ln)?\\((.*:s\\[:x\\]\\*:x.*)|(.*:x\\*:s\\[:x\\].*)\\)|||You are[ not] using :x[index]*:s[array](:x[index])||||||"
							+ "C2 ARRAY_COMPUTATION.2>>>:x=1|||Variable :x[] is[ not] initialized to 1||||||"
							+ "C3 CONDITIONAL_PRINT_CONSTANT.1>>>ARRAY_COMPUTATION.:s\\.length==1|||The condition are[ not] considering arrays of length one||||||"
							+ "C4 CONDITIONAL_PRINT_CONSTANT.2>>>System\\.out\\.print(ln)?\\(\\\"0.*\\\"\\)|||You are[ not] printing zero||||||");

		if (a == 10)
			constraintsToApply.put("main",
					"C1 PRINT_CONSOLE.1<=>CUMULATIVELY_ADD.2|||Variable :x[] is[ not] cumulatively added and printed to console||||||"
							+ "C2 ARRAY_COMPUTATION.6<=>CUMULATIVELY_ADD.2|||Variable CUMULATIVELY_ADD.:x[] is[ not] cumulatively added using ARRAY_COMPUTATION.:s[array]||||||"
							+ "C3 CUMULATIVELY_ADD.2>>>ARRAY_COMPUTATION.:s\\[ARRAY_COMPUTATION.:x\\]\\*Math\\.pow\\(:methodParameter,ARRAY_COMPUTATION.:x\\)|||You are[ not] cumulatively adding ARRAY_COMPUTATION.:s[array] and pow(:methodParameter[parameter],ARRAY_COMPUTATION.:x[index])||||||"
							+ "C4 ARRAY_COMPUTATION.2>>>:x=0|||Variable :x[] is[ not] initialized to zero||||||");

		if (a == 11)
			constraintsToApply.put("main",
					"C1 PRINT_CONSOLE.1<=>COND_CUMULATIVELY_ADD.2|||Variable :x[] is[ not] cumulatively added and printed to console||||||"
							+ "C2 ACCESS_POS_0_OUT_5_FILE.1>>>:s=newjava\\.util\\.Scanner\\(newjava\\.io\\.File\\(\"assignments/rit-all-g-medals/summer_olympics.txt\"\\)\\)|||You are[ not] initializing :s[file scanner] with file summer_olympics.txt||||||"
							+ "C3 ACCESS_POS_1_OUT_5_FILE.1>>>:s=newjava\\.util\\.Scanner\\(newjava\\.io\\.File\\(\"assignments/rit-all-g-medals/summer_olympics.txt\"\\)\\)|||You are[ not] initializing :s[file scanner] with file summer_olympics.txt||||||"
							+ "C4 ACCESS_POS_2_OUT_5_FILE.1>>>:s=newjava\\.util\\.Scanner\\(newjava\\.io\\.File\\(\"assignments/rit-all-g-medals/summer_olympics.txt\"\\)\\)|||You are[ not] initializing :s[file scanner] with file summer_olympics.txt||||||"
							+ "C5 ACCESS_POS_3_OUT_5_FILE.1>>>:s=newjava\\.util\\.Scanner\\(newjava\\.io\\.File\\(\"assignments/rit-all-g-medals/summer_olympics.txt\"\\)\\)|||You are[ not] initializing :s[file scanner] with file summer_olympics.txt||||||"
							+ "C6 ACCESS_POS_4_OUT_5_FILE.1>>>:s=newjava\\.util\\.Scanner\\(newjava\\.io\\.File\\(\"assignments/rit-all-g-medals/summer_olympics.txt\"\\)\\)|||You are[ not] initializing :s[file scanner] with file summer_olympics.txt||||||"
							+ "C7 COND_CUMULATIVELY_ADD.3>>>ACCESS_POS_2_OUT_5_FILE.:x\\%5==4\\&\\&ACCESS_POS_2_OUT_5_FILE.:y==:methodParameter\\&\\&ACCESS_POS_3_OUT_5_FILE.:y==1|||You are[ not] enforcing the condition correctly||||||");

		if (a == 12)
			constraintsToApply.put("main",
					"C1 PRINT_CONSOLE.1<=>COND_CUMULATIVELY_ADD.2|||Variable :x[] is[ not] cumulatively added and printed to console||||||"
							+ "C2 ACCESS_POS_0_OUT_5_FILE.1>>>:s=newjava\\.util\\.Scanner\\(newjava\\.io\\.File\\(\"assignments/rit-medals-by-ath/summer_olympics.txt\"\\)\\)|||You are[ not] initializing :s[file scanner] with file summer_olympics.txt||||||"
							+ "C3 ACCESS_POS_1_OUT_5_FILE.1>>>:s=newjava\\.util\\.Scanner\\(newjava\\.io\\.File\\(\"assignments/rit-medals-by-ath/summer_olympics.txt\"\\)\\)|||You are[ not] initializing :s[file scanner] with file summer_olympics.txt||||||"
							+ "C4 ACCESS_POS_2_OUT_5_FILE.1>>>:s=newjava\\.util\\.Scanner\\(newjava\\.io\\.File\\(\"assignments/rit-medals-by-ath/summer_olympics.txt\"\\)\\)|||You are[ not] initializing :s[file scanner] with file summer_olympics.txt||||||"
							+ "C5 ACCESS_POS_3_OUT_5_FILE.1>>>:s=newjava\\.util\\.Scanner\\(newjava\\.io\\.File\\(\"assignments/rit-medals-by-ath/summer_olympics.txt\"\\)\\)|||You are[ not] initializing :s[file scanner] with file summer_olympics.txt||||||"
							+ "C6 ACCESS_POS_4_OUT_5_FILE.1>>>:s=newjava\\.util\\.Scanner\\(newjava\\.io\\.File\\(\"assignments/rit-medals-by-ath/summer_olympics.txt\"\\)\\)|||You are[ not] initializing :s[file scanner] with file summer_olympics.txt||||||"
							+ "C7 COND_CUMULATIVELY_ADD.3>>>"
							+ "ACCESS_POS_2_OUT_5_FILE.:x\\%5==4\\&\\&:methodParameter\\.equals\\(ACCESS_POS_0_OUT_5_FILE.:y\\)\\&\\&:methodParameter\\.equals\\(ACCESS_POS_1_OUT_5_FILE.:y\\)\\&\\&ACCESS_POS_3_OUT_5_FILE.:y==(1|2|3)|||You are[ not] enforcing the condition correctly||||||");

		return constraintsToApply;
	}

}
